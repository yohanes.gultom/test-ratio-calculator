import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class TestRatioCalculatorTest {

    TestRatioCalculator testRatioCalculator = new TestRatioCalculator();

    @Test
    public void testCountFile() throws IOException {
        String filePath = "src/test/resources/ms-dummy/src/server.ts";
        assertEquals(84, testRatioCalculator.countLine(filePath));
    }

    @Test
    public void testIsAppFile() {
        String filePath = "src/test/resources/ms-dummy/src/server.ts";
        assertTrue(testRatioCalculator.isAppFile(filePath));
    }

    @Test
    public void testIsTestFile() {
        String filePath = "src/test/resources/ms-dummy/src/__tests__/config.spec.ts";
        assertTrue(testRatioCalculator.isTestFile(filePath));
    }

    @Test
    public void testCalculateTestRatioOnTestsFolder() throws IOException {
        String dirPath = "src/test/resources/ms-dummy/src/__tests__";
        TestRatio testRatio = testRatioCalculator.calculateTestRatio(dirPath);
        assertEquals(0, testRatio.getAppFileCount());
        assertEquals(0, testRatio.getAppLineCount());
        assertEquals(4, testRatio.getTestFileCount());
        assertEquals(195, testRatio.getTestLineCount());
    }

    @Test
    public void testCalculateTestRatioOnMocksFolder() throws IOException{
        String dirPath = "src/test/resources/ms-dummy/src/__mocks__";
        TestRatio testRatio = testRatioCalculator.calculateTestRatio(dirPath);
        assertEquals(0, testRatio.getAppFileCount());
        assertEquals(0, testRatio.getAppLineCount());
        assertEquals(2, testRatio.getTestFileCount());
        assertEquals(34, testRatio.getTestLineCount());
    }

    @Test
    public void testCalculateTestRatioOnModuleFolder() throws IOException{
        String dirPath = "src/test/resources/ms-dummy/src/entitlement";
        TestRatio testRatio = testRatioCalculator.calculateTestRatio(dirPath);
        assertEquals(8, testRatio.getAppFileCount());
        assertEquals(14, testRatio.getTestFileCount());
        assertEquals(1, testRatio.getOtherFileCount());
        assertEquals(1254, testRatio.getAppLineCount());
        assertEquals(3169, testRatio.getTestLineCount());
    }

    @Test
    public void testCalculateTestRatioOnProjectFolder() throws IOException{
        String dirPath = "src/test/resources/ms-dummy";
        TestRatio testRatio = testRatioCalculator.calculateTestRatio(dirPath);
        assertEquals(10, testRatio.getAppFileCount());
        assertEquals(20, testRatio.getTestFileCount());
        assertEquals(4, testRatio.getOtherFileCount());
        assertEquals(1372, testRatio.getAppLineCount());
        assertEquals(3398, testRatio.getTestLineCount());
    }

    @Test
    public void testGetRatio() {
        TestRatio testRatio = new TestRatio(1, 100, 1, 50, 0);
        assertEquals(0.5, testRatio.getRatio());
    }

    @Test
    public void testGetFileExtension() {
        assertEquals("json", testRatioCalculator.getFileExtension("package.json"));
        assertEquals("ts", testRatioCalculator.getFileExtension("entitlement.data.ts"));
        assertEquals("ts", testRatioCalculator.getFileExtension("__tests__/atm.withdrawal.consume.contract.spec.ts"));
    }

    @Test
    public void testGetFileExtensionOfHiddenFile() {
        assertEquals("", testRatioCalculator.getFileExtension(".gitignore"));
        assertEquals("", testRatioCalculator.getFileExtension(".dockerignore"));
    }
}
