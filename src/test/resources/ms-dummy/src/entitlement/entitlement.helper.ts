import { FeeRuleType } from '../fee/fee.constant';
import { wrapLogs } from '../logger';
import { EntitlementCode, AwardLimitGroupCodes } from './entitlement.enum';
import { IGetEntitlementRequest } from './entitlement.interface';

const constructGetUsageCounterPayload = (
  entitlementCodes: string[]
): IGetEntitlementRequest => {
  return {
    entitlementCodes: entitlementCodes
  };
};

const getFilteredLimitGroupCodes = (): string[] => {
  return [
    AwardLimitGroupCodes.BONUS_TRANSFER,
    AwardLimitGroupCodes.BONUS_LOCAL_CASH_WITHDRAWAL
  ];
};

const getFilteredEntitlementCodes = (): string[] => {
  return [
    EntitlementCode.BONUS_TRANSFER,
    EntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL
  ];
};

const getFilteredFeeRuleType = (): string[] => {
  return [
    FeeRuleType.RTOL_TRANSFER_FEE_RULE,
    FeeRuleType.SKN_TRANSFER_FEE_RULE,
    FeeRuleType.RTGS_TRANSFER_FEE_RULE,
    FeeRuleType.LOCAL_ATM_WITHDRAWAL_RULE,
    FeeRuleType.WALLET_FEE_RULE,
    FeeRuleType.BIFAST_FEE_RULE,
    FeeRuleType.BIFAST_SHARIA_FEE_RULE
  ];
};

const entitlementHelper = wrapLogs({
  constructGetUsageCounterPayload,
  getFilteredEntitlementCodes,
  getFilteredLimitGroupCodes,
  getFilteredFeeRuleType
});

export default entitlementHelper;
