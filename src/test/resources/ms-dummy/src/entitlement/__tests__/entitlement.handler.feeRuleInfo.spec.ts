import { FeeRuleType } from '../../fee/fee.constant';
import { EntitlementHandler } from '../entitlement.handler';
import {
  createBiFastFeeRule,
  createLocalAtmWithdrawalRule,
  createRtolFeeRuleInfo,
  createWalletFeeRule,
  mockBIFastScenario,
  mockLocalAtmWithdrawalScenario,
  mockOverseasAtmWithdrawalScenario,
  mockRtolTransferScenario,
  mockWalletScenario
} from '../__mocks__/entitlement.handler.mock.data';

describe('entitlement handler', () => {
  describe('RTOL_TRANSFER_FEE_RULE', () => {
    const transferFeeQuota = 25;

    it.each([
      [mockRtolTransferScenario(transferFeeQuota).withinQuota()],
      [mockRtolTransferScenario(transferFeeQuota).withinQuota2()],
      [mockRtolTransferScenario(transferFeeQuota).exceedQuota()]
    ])(`'should be modified successfully'`, scenarioParams => {
      const entitlementHandler = new EntitlementHandler();

      if (scenarioParams) {
        const feeRuleInfo = createRtolFeeRuleInfo(scenarioParams.paramsFeeRule);

        const updatedFeeRuleInfo = entitlementHandler.processFeeRuleInfo(
          feeRuleInfo,
          FeeRuleType.RTOL_TRANSFER_FEE_RULE,
          scenarioParams.paramsMappedEntitlement.awardGroupCounter,
          scenarioParams.paramsMappedEntitlement.usageCounters
        );

        expect(updatedFeeRuleInfo).toEqual(scenarioParams.output);
      }
    });
  });

  describe('LOCAL_ATM_WITHDRAWAL_RULE ', () => {
    const atmWithdrawalQuota = 25;

    it.each([
      [mockLocalAtmWithdrawalScenario(atmWithdrawalQuota).withinQuota()],
      [mockLocalAtmWithdrawalScenario(atmWithdrawalQuota).withinQuota2()],
      [mockLocalAtmWithdrawalScenario(atmWithdrawalQuota).exceedQuota()]
    ])(`'should be modified successfully'`, scenarioParams => {
      const entitlementHandler = new EntitlementHandler();

      if (scenarioParams) {
        const feeRuleInfo = createLocalAtmWithdrawalRule(
          scenarioParams.paramsFeeRule
        );

        const updatedFeeRuleInfo = entitlementHandler.processFeeRuleInfo(
          feeRuleInfo,
          FeeRuleType.LOCAL_ATM_WITHDRAWAL_RULE,
          scenarioParams.paramsMappedEntitlement.awardGroupCounter,
          scenarioParams.paramsMappedEntitlement.usageCounters
        );

        expect(updatedFeeRuleInfo).toEqual(scenarioParams.output);
      }
    });
  });

  describe('WALLET_FEE_RULE ', () => {
    const walletQuota = 25;

    it.each([
      [mockWalletScenario(walletQuota).withinQuota()],
      [mockWalletScenario(walletQuota).withinQuota2()],
      [mockWalletScenario(walletQuota).exceedQuota()]
    ])(`'should be modified successfully'`, scenarioParams => {
      const entitlementHandler = new EntitlementHandler();

      if (scenarioParams) {
        const feeRuleInfo = createWalletFeeRule(scenarioParams.paramsFeeRule);

        const updatedFeeRuleInfo = entitlementHandler.processFeeRuleInfo(
          feeRuleInfo,
          FeeRuleType.WALLET_FEE_RULE,
          scenarioParams.paramsMappedEntitlement.awardGroupCounter,
          scenarioParams.paramsMappedEntitlement.usageCounters
        );

        expect(updatedFeeRuleInfo).toEqual(scenarioParams.output);
      }
    });
  });

  describe('BIFAST_FEE_RULE ', () => {
    const bifastQuota = 25;

    it.each([
      [mockBIFastScenario(bifastQuota).withinQuota()],
      [mockBIFastScenario(bifastQuota).withinQuota2()],
      [mockBIFastScenario(bifastQuota).exceedQuota()]
    ])(`'should be modified successfully'`, scenarioParams => {
      const entitlementHandler = new EntitlementHandler();

      if (scenarioParams) {
        const feeRuleInfo = createBiFastFeeRule(scenarioParams.paramsFeeRule);

        const updatedFeeRuleInfo = entitlementHandler.processFeeRuleInfo(
          feeRuleInfo,
          FeeRuleType.BIFAST_FEE_RULE,
          scenarioParams.paramsMappedEntitlement.awardGroupCounter,
          scenarioParams.paramsMappedEntitlement.usageCounters
        );

        expect(updatedFeeRuleInfo).toEqual(scenarioParams.output);
      }
    });
  });

  describe('OVERSEAS_ATM_WITHDRAWAL_RULE ', () => {
    const overseasAtmWithdrawalQuota = 25;

    it.each([
      [
        mockOverseasAtmWithdrawalScenario(
          overseasAtmWithdrawalQuota
        ).withinQuota()
      ],
      [
        mockOverseasAtmWithdrawalScenario(
          overseasAtmWithdrawalQuota
        ).withinQuota2()
      ],
      [
        mockOverseasAtmWithdrawalScenario(
          overseasAtmWithdrawalQuota
        ).exceedQuota()
      ]
    ])(`'should be modified successfully'`, scenarioParams => {
      const entitlementHandler = new EntitlementHandler();

      if (scenarioParams) {
        const feeRuleInfo = createBiFastFeeRule(scenarioParams.paramsFeeRule);

        const updatedFeeRuleInfo = entitlementHandler.processFeeRuleInfo(
          feeRuleInfo,
          FeeRuleType.OVERSEAS_ATM_WITHDRAWAL_RULE,
          scenarioParams.paramsMappedEntitlement.awardGroupCounter,
          scenarioParams.paramsMappedEntitlement.usageCounters
        );

        expect(updatedFeeRuleInfo).toEqual(feeRuleInfo);
      }
    });
  });
});
