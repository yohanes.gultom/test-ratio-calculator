import { FeeRuleType } from '../../fee/fee.constant';
import { AwardLimitGroupCodes, EntitlementCode } from '../entitlement.enum';
import entitlementHelper from '../entitlement.helper';

describe('entitlement.helper', () => {
  describe('constructGetUsageCounterPayload', () => {
    it('should return IGetEntitlementRequest', async () => {
      const result = entitlementHelper.constructGetUsageCounterPayload([
        'count.free.transfer.out'
      ]);
      expect(result).toStrictEqual({
        entitlementCodes: ['count.free.transfer.out']
      });
    });
  });

  describe('getFilteredLimitGroupCodes', () => {
    it('should return Bonus_Transfer And Bonus_Local_CashWithdrawal', async () => {
      const result = entitlementHelper.getFilteredLimitGroupCodes();
      expect(result).toStrictEqual([
        AwardLimitGroupCodes.BONUS_TRANSFER,
        AwardLimitGroupCodes.BONUS_LOCAL_CASH_WITHDRAWAL
      ]);
    });
  });

  describe('getFilteredEntitlementCodes', () => {
    it('should return IGetEntitlementRequest', async () => {
      const result = entitlementHelper.getFilteredEntitlementCodes();
      expect(result).toStrictEqual([
        EntitlementCode.BONUS_TRANSFER,
        EntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL
      ]);
    });
  });

  describe('getFilteredFeeRuleType', () => {
    it('should return IGetEntitlementRequest', async () => {
      const result = entitlementHelper.getFilteredFeeRuleType();
      expect(result).toStrictEqual([
        FeeRuleType.RTOL_TRANSFER_FEE_RULE,
        FeeRuleType.SKN_TRANSFER_FEE_RULE,
        FeeRuleType.RTGS_TRANSFER_FEE_RULE,
        FeeRuleType.LOCAL_ATM_WITHDRAWAL_RULE,
        FeeRuleType.WALLET_FEE_RULE,
        FeeRuleType.BIFAST_FEE_RULE,
        FeeRuleType.BIFAST_SHARIA_FEE_RULE
      ]);
    });
  });
});
