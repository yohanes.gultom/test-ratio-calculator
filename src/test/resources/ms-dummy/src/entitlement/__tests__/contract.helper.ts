import { Matchers } from '@pact-foundation/pact';
import * as uuidGen from 'uuid';

import {
  ICounterResponse,
  ICounterUsageRequest,
  IEntitlementResponse,
  IUpdateUsageCounterEntitlementResponse
} from '../entitlement.interface';
import { ICounterTemplate, IEntitlementTemplate } from './contract.interface';

const { like, string, decimal, boolean, uuid } = Matchers;

const createCounterUsageObj = (params: {
  counterCode: string;
  usedQuota: number;
}): ICounterUsageRequest => {
  return {
    id: uuidGen.v4(),
    counterCode: params.counterCode,
    usedQuota: params.usedQuota
  };
};

const convertCounterResponseToMap = (counters: ICounterResponse[]) => {
  const result = new Map(counters.map(counter => [counter.id, counter]));
  return result;
};

const convertExpectedOutputEntitlementToMatcher = (
  entitlementResponse: IEntitlementResponse[]
): object => {
  const emptyTemplate: IEntitlementTemplate = {
    entitlement: {},
    type: {},
    dataType: {},
    description: {},
    quota: {},
    comments: {}
  };

  const result: IEntitlementTemplate[] = entitlementResponse.map(
    entitlement => {
      const updatedEntitlementTemplate = Object.assign({}, emptyTemplate);
      updatedEntitlementTemplate.entitlement = string(entitlement.entitlement);
      updatedEntitlementTemplate.type = string(entitlement.type); //sample data
      updatedEntitlementTemplate.dataType = string(entitlement.dataType); //sample data
      updatedEntitlementTemplate.description = string(entitlement.description);
      updatedEntitlementTemplate.quota = decimal(entitlement.quota as number);
      updatedEntitlementTemplate.comments = string(entitlement.comments);
      if (entitlement.usedQuota) {
        updatedEntitlementTemplate.usedQuota = decimal(entitlement.usedQuota);
      }

      if (entitlement.counterCode) {
        updatedEntitlementTemplate.counterCode = string(
          entitlement.counterCode
        );
      }

      if (entitlement.value) {
        updatedEntitlementTemplate.value = string(entitlement.value);
      }

      return updatedEntitlementTemplate;
    }
  );

  //return eachLike(result[0]);
  return result;
};

const convertExpectedOutputToCounterMatcher = (
  expectedOutputRaw: ICounterResponse[]
): object => {
  const emptyTemplate: ICounterTemplate = {
    id: {},
    recorded: {}
  };

  const result: ICounterTemplate[] = expectedOutputRaw.map(d => {
    const updatedCounterTemplate = Object.assign({}, emptyTemplate);
    updatedCounterTemplate.id = uuid(d.id);
    updatedCounterTemplate.recorded = boolean(d.recorded);
    if (d.reason) {
      updatedCounterTemplate.reason = like({
        type: d.reason.type,
        description: d.reason.description
      });
    }

    return updatedCounterTemplate;
  });

  return result;
};

const convertExpectedOutputToMatcher = (
  expectedOutputs: IUpdateUsageCounterEntitlementResponse
): object => {
  const entitlementResult = convertExpectedOutputEntitlementToMatcher(
    expectedOutputs.entitlements
  );

  const counterResult = convertExpectedOutputToCounterMatcher(
    expectedOutputs.counters
  );

  return {
    entitlements: entitlementResult,
    counters: counterResult
  };
};

const ContractHelper = {
  createCounterUsageObj,
  convertCounterResponseToMap,
  convertExpectedOutputToMatcher
};

export default ContractHelper;
