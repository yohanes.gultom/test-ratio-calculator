export interface IPactInitRequest {
  providerName: string;
  consumerName: string;
  portNumber: number;
}

export interface IContractTestExistingState {
  entitlementCode: string;
  counterCode: string;
  quota: number;
  usedQuota: number;
}

export interface IEntitlementTemplate {
  entitlement: object;
  type: object;
  dataType: object;
  description: object;
  quota: object;
  comments: object;
  usedQuota?: object;
  counterCode?: object;
  value?: object;
}

export interface ICounterTemplate {
  id: object;
  recorded: object;
  reason?: object;
}
