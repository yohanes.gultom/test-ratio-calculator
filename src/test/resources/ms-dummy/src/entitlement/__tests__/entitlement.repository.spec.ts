import httpClient from '../httpClient';
import entitlementRepository from '../entitlement.repository';
import { AppError } from '../../errors/AppError';

jest.mock('../httpClient');

describe('entitlement.repository', () => {
  it('should be getEntitlementUsageCounters successfully', async () => {
    const customerId = 'customer-id';
    const entitlementCodes = ['transfer'];
    (httpClient.post as jest.Mock).mockResolvedValue({
      data: {
        entitlements: [
          {
            entitlement: 'count.free.atm_withdrawal',
            type: 'MONTHLY_COUNTER',
            dataType: 'INTEGER',
            description: 'Free ATM Withdrawal',
            quota: 3,
            comments: 'Free ATM withdrawals',
            usedQuota: 1,
            counterCode: 'counter.free.atm_withdrawal'
          }
        ]
      }
    });
    const response = await entitlementRepository.getEntitlementUsageCounters(
      customerId,
      entitlementCodes
    );
    expect(httpClient.post).toHaveBeenCalledWith(
      `/private/${customerId}/counter`,
      {
        entitlementCodes
      }
    );
  });
  it('should be getEntitlementUsageCounters failed', async () => {
    const customerId = 'customer-id';
    const entitlementCodes = ['transfer'];
    (httpClient.post as jest.Mock).mockRejectedValueOnce({});
    try {
      const response = await entitlementRepository.getEntitlementUsageCounters(
        customerId,
        entitlementCodes
      );
    } catch (error) {
      expect(error).toBeInstanceOf(AppError);
    }
  });

  it('should be updateEntitlementUsageCounters successfully', async () => {
    const request = {
      customerId: 'customerId',
      entitlementCodes: ['transfer'],
      counterUsages: [
        {
          id: 'id',
          counterCode: 'counterCode',
          usedQuota: 1
        }
      ]
    };
    (httpClient.post as jest.Mock).mockResolvedValue({
      data: {
        entitlements: [
          {
            entitlement: 'count.free.atm_withdrawal',
            type: 'MONTHLY_COUNTER',
            dataType: 'INTEGER',
            description: 'Free ATM Withdrawal',
            quota: 3,
            comments: 'Free ATM withdrawals',
            usedQuota: 1,
            counterCode: 'counter.free.atm_withdrawal'
          }
        ],
        counters: [
          {
            id: 'feae5a3c-000c-40d0-9b89-bfed0290fe4b',
            recorded: true
          }
        ]
      }
    });
    const response = await entitlementRepository.updateEntitlementUsageCounters(
      request
    );
    const { entitlementCodes, counterUsages } = request;
    expect(httpClient.post).toHaveBeenCalledWith(
      `/private/${request.customerId}/counter`,
      {
        entitlementCodes,
        counterUsages
      }
    );
  });

  it('should be revertEntitlementUsageCounters successfully', async () => {
    const request = {
      customerId: 'customerId',
      entitlementCodes: ['transfer'],
      counterReverts: [
        {
          id: 'id',
          counterCode: 'counterCode'
        }
      ]
    };
    (httpClient.post as jest.Mock).mockImplementationOnce(() =>
      Promise.resolve({})
    );
    const response = await entitlementRepository.revertEntitlementUsageCounters(
      request
    );
    const { entitlementCodes, counterReverts } = request;
    expect(httpClient.post).toHaveBeenCalledWith(
      `/private/${request.customerId}/counter`,
      {
        entitlementCodes,
        counterReverts
      }
    );
  });

  it('should throw error when get empty data', async () => {
    const customerId = 'customer-id';
    const entitlementCodes = ['transfer'];
    (httpClient.post as jest.Mock).mockResolvedValueOnce({});
    try {
      await entitlementRepository.getEntitlementUsageCounters(
        customerId,
        entitlementCodes
      );
    } catch (error) {
      expect(error).toBeInstanceOf(AppError);
    }
  });

  it('should throw error when update with incorrect entitlement code', async () => {
    const request = {
      customerId: 'customerId',
      entitlementCodes: ['transfer'],
      counterUsages: [
        {
          id: 'id',
          counterCode: 'counterCode',
          usedQuota: 1
        }
      ]
    };
    (httpClient.post as jest.Mock).mockResolvedValue({
      data: {
        counters: [
          {
            id: 'feae5a3c-000c-40d0-9b89-bfed0290fe4b',
            recorded: true
          }
        ]
      }
    });
    try {
      await entitlementRepository.updateEntitlementUsageCounters(request);
    } catch (error) {
      expect(error).toBeInstanceOf(AppError);
    }
  });

  it('should throw error when update with invalid counter code', async () => {
    const request = {
      customerId: 'customerId',
      entitlementCodes: ['transfer'],
      counterUsages: [
        {
          id: 'id',
          counterCode: 'counterCode',
          usedQuota: 1
        }
      ]
    };
    (httpClient.post as jest.Mock).mockResolvedValue({
      data: {
        entitlements: [
          {
            entitlement: 'count.free.atm_withdrawal',
            type: 'MONTHLY_COUNTER',
            dataType: 'INTEGER',
            description: 'Free ATM Withdrawal',
            quota: 3,
            comments: 'Free ATM withdrawals',
            usedQuota: 1,
            counterCode: 'counter.free.atm_withdrawal'
          }
        ],
        counters: [
          {
            id: 'feae5a3c-000c-40d0-9b89-bfed0290fe4b',
            recorded: false,
            reason: {
              type: 'notFound'
            }
          }
        ]
      }
    });
    try {
      await entitlementRepository.updateEntitlementUsageCounters(request);
    } catch (error) {
      expect(error).toBeInstanceOf(AppError);
    }
  });
});
