const CONSUMER_NAME = 'ms-transfer';
const PROVIDER_NAME = 'ms-entitlement';
const PORT_NUMBER = 49151;

export { CONSUMER_NAME, PROVIDER_NAME, PORT_NUMBER };
