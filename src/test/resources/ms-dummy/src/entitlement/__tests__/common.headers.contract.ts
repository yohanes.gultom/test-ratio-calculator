import { Pact } from '@pact-foundation/pact';
import path from 'path';
import { IPactInitRequest } from './contract.interface';

const jestMockInit = () => {
  jest.mock('../../logger');
  jest.mock('../httpClient', () => {
    const httpClient = require('@dk/module-httpclient');
    const context = require('../../context').context;
    const logger = {
      error: jest.fn(), // error: e => console.error(e),
      info: jest.fn(), // info: e => console.info(e),
      debug: jest.fn(), // debug: e => console.info(e),
      warn: jest.fn(), // warn: e => console.warn(e),
      critical: jest.fn() // critical: e => console.error(e)
    };
    return httpClient.createHttpClient({
      baseURL: 'http://localhost:49151',
      context,
      logger
    });
  });
};

const initPactProvider = (params: IPactInitRequest) => {
  const { consumerName, providerName, portNumber } = params;

  const provider = new Pact({
    consumer: consumerName,
    provider: providerName,
    port: portNumber,
    log: path.resolve(process.cwd(), 'logs', 'pact.log'),
    dir: path.resolve(process.cwd(), 'pacts'),
    logLevel: 'info'
  });

  return provider;
};

const contractCommonInitFn = { jestMockInit, initPactProvider };

export default contractCommonInitFn;
