import { IErrorDetail } from '@dk/module-common/dist/error/error.interface';
import _ from 'lodash';
import { get } from 'lodash';
import { ERROR_CODE } from '../common/errors';
import { AppError } from '../errors/AppError';
import logger, { wrapLogs } from '../logger';
import { ENTITLEMENT_API_ENDPOINT } from './entitlement.constant';
import { EntitlementErrorReasonType } from './entitlement.enum';
import entitlementHelper from './entitlement.helper';
import {
  ICounterRevertEntitlementRequest,
  ICounterRevertEntitlementResponse,
  IEntitlementResponse,
  IUpdateUsageCounterEntitlementRequest,
  IUpdateUsageCounterEntitlementResponse
} from './entitlement.interface';

import httpClient from './httpClient';

const getEntitlementUsageCounters = async (
  customerId: string,
  entitlementCodes: string[]
): Promise<IEntitlementResponse[] | []> => {
  try {
    logger.info(
      `getEntitlementUsageCounters from ms-entitlement: get usage counter for customerId: ${customerId}.`
    );
    const endpoint = ENTITLEMENT_API_ENDPOINT(customerId);
    const payload = entitlementHelper.constructGetUsageCounterPayload(
      entitlementCodes
    );
    const result = await httpClient.post(endpoint, payload);

    if (_.isEmpty(result)) {
      logger.error(
        `getEntitlementUsageCounters: failed to retrieve entitlement for 
      ${entitlementCodes.toString()}
      `
      );
      throw new AppError(ERROR_CODE.FAILED_TO_RETRIEVE_ENTITLEMENT);
    }

    return get(result, ['data', 'entitlements'], []);
  } catch (error) {
    logger.error(
      `getEntitlementUsageCounters: error when get user atm withdrawal quota. ${error.message}`
    );
    const errorDetail: IErrorDetail[] = [];
    errorDetail.push({
      message: (error && error.message) || '',
      key: error.code,
      code: (error && error.status) || ''
    });
    throw new AppError(error.code, errorDetail);
  }
};

const updateEntitlementUsageCounters = async (
  params: IUpdateUsageCounterEntitlementRequest
): Promise<IUpdateUsageCounterEntitlementResponse> => {
  const { customerId, entitlementCodes, counterUsages } = params;

  logger.info(
    `updateEntitlementUsageCounters [ms-entitlement] for customerId: ${customerId}.`
  );

  const endpoint = ENTITLEMENT_API_ENDPOINT(customerId);

  const result = await httpClient.post(endpoint, {
    entitlementCodes,
    counterUsages
  });

  if (
    !result.data.hasOwnProperty('entitlements') ||
    (!result.data.counters[0].recorded &&
      result.data.counters[0].reason.type !==
        EntitlementErrorReasonType.QUOTA_LIMIT)
  ) {
    const infos = counterUsages
      .map(
        counterUsage =>
          `[id: ${counterUsage.id} AND counterCode: ${counterUsage.counterCode} AND usedQuota: ${counterUsage.usedQuota}]`
      )
      .join(', ');

    logger.error(
      `updateEntitlementUsageCounters: failed to update entitlement for 
      ${infos}
      `
    );
    throw new AppError(ERROR_CODE.FAILED_TO_UPDATE_ENTITLEMENT);
  }

  return result?.data;
};

const revertEntitlementUsageCounters = async (
  params: ICounterRevertEntitlementRequest
): Promise<ICounterRevertEntitlementResponse> => {
  const { customerId, entitlementCodes, counterReverts } = params;

  logger.info(
    `revertEntitlementUsageCounters [ms-entitlement] for customerId: ${customerId}.`
  );

  const endpoint = ENTITLEMENT_API_ENDPOINT(customerId);

  const result = await httpClient.post(endpoint, {
    entitlementCodes,
    counterReverts
  });

  if (!result) {
    const infos = counterReverts
      .map(
        counterRevert =>
          `[id: ${counterRevert.id} AND counterCode: ${counterRevert.counterCode}]`
      )
      .join(', ');

    logger.error(
      `revertEntitlementUsageCounters: failed to revert entitlement for 
      ${infos}
      `
    );
    throw new AppError(ERROR_CODE.FAILED_TO_REVERT_ENTITLEMENT);
  }

  return result?.data;
};

const entitlementRepository = wrapLogs({
  getEntitlementUsageCounters,
  updateEntitlementUsageCounters,
  revertEntitlementUsageCounters
});

export default entitlementRepository;
