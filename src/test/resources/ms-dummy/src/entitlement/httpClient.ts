import { HttpClient } from '@dk/module-httpclient';
import { config } from '../config';
import logger from '../logger';
import { context } from '../context';
import { Http } from '@dk/module-common';
import { retryConfig } from '../common/constant';

const MS_ENTITLEMENT_URL = config.get('ms').entitlement;

const httpClient: HttpClient = Http.createHttpClientForward({
  baseURL: MS_ENTITLEMENT_URL,
  context,
  logger,
  retryConfig
});

logger.info(
  `Setting up http-client with ms-entitlement on address : ${MS_ENTITLEMENT_URL}`
);

export default httpClient;
