export const ENTITLEMENT_API_ENDPOINT = (customerId: string) => {
  return `/private/${customerId}/counter`;
};
