export enum MockEntitlementCode {
  BONUS_TRANSFER = 'count.free.transfer.out',
  BONUS_LOCAL_CASH_WITHDRAWAL = 'count.free.atm_withdrawal'
}

export enum MockEntitlementCounterCode {
  COUNTER_BONUS_TRANSFER = 'counter.free.transfer.out',
  COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL = 'counter.free.atm_withdrawal'
}
