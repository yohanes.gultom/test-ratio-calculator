import { BankNetworkEnum } from '@dk/module-common';
import { UsageCounter } from '../../award/award.type';
import { FeeRuleType, COUNTER } from '../../fee/fee.constant';
import { FeeRuleInfo } from '../../fee/fee.type';
import { AwardLimitGroupCodes } from '../entitlement.enum';

export interface IParamsFeeRule {
  code: FeeRuleType;
  transactionAmount: number;
  monthlyNoTransaction: number;
  interchange: BankNetworkEnum;
  counterCode: COUNTER;
}

export interface IParamsMappedEntitlement {
  awardGroupCounter: AwardLimitGroupCodes;
  usageCounters: UsageCounter[];
}

export interface IMockFeeRule {
  paramsFeeRule: IParamsFeeRule;
  paramsMappedEntitlement: IParamsMappedEntitlement;
  output: FeeRuleInfo;
}

export const createRtolFeeRuleInfo = (request: IParamsFeeRule) => {
  const rtolTransferFeeRuleObject: FeeRuleInfo = {
    code: request.code,
    counterCode: request.counterCode,
    basicFeeCode1: [
      {
        interchange: BankNetworkEnum.ARTAJASA,
        basicFeeCode: 'TF015'
      },
      {
        interchange: BankNetworkEnum.ALTO,
        basicFeeCode: 'TF008'
      }
    ],
    basicFeeCode2: [
      {
        interchange: BankNetworkEnum.ARTAJASA,
        basicFeeCode: 'TF010'
      },
      {
        interchange: BankNetworkEnum.ALTO,
        basicFeeCode: 'TF009'
      }
    ],
    monthlyNoTransaction: request.monthlyNoTransaction,
    interchange: request.interchange
  };

  return rtolTransferFeeRuleObject;
};

export const createLocalAtmWithdrawalRule = (request: IParamsFeeRule) => {
  const localAtmFeeRuleInfo: FeeRuleInfo = {
    code: request.code,
    counterCode: COUNTER.COUNTER_07,
    basicFeeCode1: [
      {
        interchange: BankNetworkEnum.ARTAJASA,
        basicFeeCode: 'CW007'
      },
      {
        interchange: BankNetworkEnum.ALTO,
        basicFeeCode: 'CW005'
      }
    ],
    basicFeeCode2: [
      {
        interchange: BankNetworkEnum.ARTAJASA,
        basicFeeCode: 'CW001'
      },
      {
        interchange: BankNetworkEnum.ALTO,
        basicFeeCode: 'CW002'
      }
    ],
    monthlyNoTransaction: request.monthlyNoTransaction,
    interchange: request.interchange
  };

  return localAtmFeeRuleInfo;
};

export const createOverseasAtmWithdrawalRule = (request: IParamsFeeRule) => {
  const localAtmFeeRuleInfo: FeeRuleInfo = {
    code: request.code,
    counterCode: COUNTER.COUNTER_07,
    basicFeeCode1: [
      {
        interchange: BankNetworkEnum.ARTAJASA,
        basicFeeCode: 'CW007'
      },
      {
        interchange: BankNetworkEnum.ALTO,
        basicFeeCode: 'CW005'
      }
    ],
    basicFeeCode2: [
      {
        interchange: BankNetworkEnum.ARTAJASA,
        basicFeeCode: 'CW001'
      },
      {
        interchange: BankNetworkEnum.ALTO,
        basicFeeCode: 'CW002'
      }
    ],
    monthlyNoTransaction: request.monthlyNoTransaction,
    interchange: request.interchange
  };

  return localAtmFeeRuleInfo;
};

export const createBiFastFeeRule = (request: IParamsFeeRule) => {
  const biFastFeeRuleObject: FeeRuleInfo = {
    code: request.code,
    counterCode: COUNTER.COUNTER_01,
    basicFeeCode1: [
      {
        basicFeeCode: 'TF022'
      }
    ],
    basicFeeCode2: [
      {
        basicFeeCode: 'TF020'
      }
    ],
    monthlyNoTransaction: request.monthlyNoTransaction,
    interchange: request.interchange
  };

  return biFastFeeRuleObject;
};

export const createWalletFeeRule = (request: IParamsFeeRule) => {
  const walletFeeRuleObject: FeeRuleInfo = {
    code: request.code,
    counterCode: COUNTER.COUNTER_01,
    basicFeeCode1: [
      {
        interchange: BankNetworkEnum.ARTAJASA,
        basicFeeCode: 'BF011'
      },
      {
        interchange: BankNetworkEnum.ALTO,
        basicFeeCode: 'BF004'
      }
    ],
    basicFeeCode2: [
      {
        interchange: BankNetworkEnum.ARTAJASA,
        basicFeeCode: 'BF005'
      },
      {
        interchange: BankNetworkEnum.ALTO,
        basicFeeCode: 'BF003'
      }
    ],
    monthlyNoTransaction: request.monthlyNoTransaction,
    interchange: request.interchange
  };

  return walletFeeRuleObject;
};

const generateFeeRuleInfoScenarioParams = (
  quota: number,
  paramsFeeRule: IParamsFeeRule,
  paramsMappedEntitlement: IParamsMappedEntitlement
) => {
  const updatedParamsFeeRule = {
    ...paramsFeeRule,
    quota: quota
  };

  const updatedParamsMappedEntitlement = {
    ...paramsMappedEntitlement
  };
  updatedParamsMappedEntitlement.usageCounters[0].quota = quota;
  updatedParamsMappedEntitlement.usageCounters[1].quota = quota;

  return {
    //scenario 1 - where used quota < quota
    withinQuota: {
      paramsFeeRule: () => {
        return { ...updatedParamsFeeRule };
      },
      paramsMappedEntitlement: () => {
        const output = { ...updatedParamsMappedEntitlement };
        output.usageCounters[0].monthlyAccumulationTransaction = 1;
        output.usageCounters[1].monthlyAccumulationTransaction = 1;
        return output;
      }
    },
    //scenario 2 - where used quota == quota, but not exceeded yet
    withinQuota2: {
      paramsFeeRule: () => {
        return {
          ...updatedParamsFeeRule,
          monthlyNoTransaction: 24
        };
      },
      paramsMappedEntitlement: () => {
        const output = { ...updatedParamsMappedEntitlement };
        output.usageCounters[0].monthlyAccumulationTransaction = 25;
        output.usageCounters[1].monthlyAccumulationTransaction = 25;
        return output;
      }
    },
    //scenario 3 - where used quota == quota, but exceeded
    exceedQuota: {
      paramsFeeRule: () => {
        return {
          ...updatedParamsFeeRule,
          monthlyNoTransaction: 25
        };
      },
      paramsMappedEntitlement: () => {
        const output = { ...updatedParamsMappedEntitlement };
        output.usageCounters[0].monthlyAccumulationTransaction = 25;
        output.usageCounters[0].isQuotaExceeded = true;

        output.usageCounters[1].monthlyAccumulationTransaction = 25;
        output.usageCounters[1].isQuotaExceeded = true;

        return output;
      }
    }
  };
};

const generateFeeRuleInfoScenarioOutput = (
  paramsFeeRule: IParamsFeeRule,
  paramsMappedEntitlement: IParamsMappedEntitlement
): IMockFeeRule | null => {
  let feeRuleInfoTemp: FeeRuleInfo | null = null;

  switch (paramsFeeRule.code) {
    case FeeRuleType.RTOL_TRANSFER_FEE_RULE:
      feeRuleInfoTemp = createRtolFeeRuleInfo(paramsFeeRule);
      break;
    case FeeRuleType.LOCAL_ATM_WITHDRAWAL_RULE:
      feeRuleInfoTemp = createLocalAtmWithdrawalRule(paramsFeeRule);
      break;
    case FeeRuleType.WALLET_FEE_RULE:
      feeRuleInfoTemp = createWalletFeeRule(paramsFeeRule);
      break;
    case FeeRuleType.BIFAST_FEE_RULE:
      feeRuleInfoTemp = createBiFastFeeRule(paramsFeeRule);
      break;
    case FeeRuleType.OVERSEAS_ATM_WITHDRAWAL_RULE:
      feeRuleInfoTemp = createOverseasAtmWithdrawalRule(paramsFeeRule);
      break;
  }
  // feeRuleInfo = createRtolFeeRuleInfo(paramsFeeRule);
  const { usageCounters } = paramsMappedEntitlement;

  if (feeRuleInfoTemp != null) {
    const feeRuleInfo = { ...feeRuleInfoTemp };
    return {
      paramsFeeRule,
      paramsMappedEntitlement,
      output: {
        ...feeRuleInfo,
        quota: usageCounters[0].quota,
        monthlyNoTransaction: usageCounters[0].monthlyAccumulationTransaction,
        isQuotaExceeded: usageCounters[0].isQuotaExceeded
      }
    };
  }
  return null;
};

/************************
 * RTOL TRANSFER
 *************************/

export const mockRtolTransferScenario = (transferFeeQuota: number) => {
  const paramsFeeTemplate: IParamsFeeRule = {
    code: FeeRuleType.RTOL_TRANSFER_FEE_RULE,
    transactionAmount: 50000,
    monthlyNoTransaction: 0,
    interchange: BankNetworkEnum.IRIS,
    counterCode: COUNTER.COUNTER_01
  };

  const paramsMappedEntitlementTemplate: IParamsMappedEntitlement = {
    awardGroupCounter: AwardLimitGroupCodes.BONUS_TRANSFER,
    usageCounters: [
      //usage counters should be already mapped with result from entitlement at this point
      {
        limitGroupCode: 'L002',
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      },
      {
        limitGroupCode: AwardLimitGroupCodes.BONUS_TRANSFER,
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      }
    ]
  };

  const {
    withinQuota: withinQuotaScenario,
    withinQuota2: withinQuotaScenario2,
    exceedQuota: exceedQuota
  } = generateFeeRuleInfoScenarioParams(
    transferFeeQuota,
    paramsFeeTemplate,
    paramsMappedEntitlementTemplate
  );

  return {
    withinQuota: () => {
      return generateFeeRuleInfoScenarioOutput(
        withinQuotaScenario.paramsFeeRule(),
        withinQuotaScenario.paramsMappedEntitlement()
      );
    },
    withinQuota2: () => {
      return generateFeeRuleInfoScenarioOutput(
        withinQuotaScenario2.paramsFeeRule(),
        withinQuotaScenario2.paramsMappedEntitlement()
      );
    },
    exceedQuota: () => {
      return generateFeeRuleInfoScenarioOutput(
        exceedQuota.paramsFeeRule(),
        exceedQuota.paramsMappedEntitlement()
      );
    }
  };
};
/************************
 * LOCAL ATM WITHDRAWAL
 ************************/

export const mockLocalAtmWithdrawalScenario = (
  localAtmWithdrawalQuota: number
) => {
  const paramsFeeRuleTemplate: IParamsFeeRule = {
    code: FeeRuleType.LOCAL_ATM_WITHDRAWAL_RULE,
    transactionAmount: 50000,
    monthlyNoTransaction: 0,
    interchange: BankNetworkEnum.ALTO,
    counterCode: COUNTER.COUNTER_07
  };

  const paramsMappedEntitlementTemplate: IParamsMappedEntitlement = {
    awardGroupCounter: AwardLimitGroupCodes.BONUS_LOCAL_CASH_WITHDRAWAL,
    usageCounters: [
      //usage counters should be already mapped with result from entitlement at this point
      {
        limitGroupCode: 'L006',
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      },
      {
        limitGroupCode: AwardLimitGroupCodes.BONUS_LOCAL_CASH_WITHDRAWAL,
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      }
    ]
  };

  const {
    withinQuota: withinQuotaScenario,
    withinQuota2: withinQuotaScenario2,
    exceedQuota: exceedQuota
  } = generateFeeRuleInfoScenarioParams(
    localAtmWithdrawalQuota,
    paramsFeeRuleTemplate,
    paramsMappedEntitlementTemplate
  );

  return {
    withinQuota: () => {
      return generateFeeRuleInfoScenarioOutput(
        withinQuotaScenario.paramsFeeRule(),
        withinQuotaScenario.paramsMappedEntitlement()
      );
    },
    withinQuota2: () => {
      return generateFeeRuleInfoScenarioOutput(
        withinQuotaScenario2.paramsFeeRule(),
        withinQuotaScenario2.paramsMappedEntitlement()
      );
    },
    exceedQuota: () => {
      return generateFeeRuleInfoScenarioOutput(
        exceedQuota.paramsFeeRule(),
        exceedQuota.paramsMappedEntitlement()
      );
    }
  };
};

/************************
 * OVERSEAS ATM WITHDRAWAL
 ************************/

export const mockOverseasAtmWithdrawalScenario = (
  overseasAtmWithdrawalQuota: number
) => {
  const paramsFeeRuleTemplate: IParamsFeeRule = {
    code: FeeRuleType.OVERSEAS_ATM_WITHDRAWAL_RULE,
    transactionAmount: 50000,
    monthlyNoTransaction: 0,
    interchange: BankNetworkEnum.ALTO,
    counterCode: COUNTER.COUNTER_07
  };

  const paramsMappedEntitlementTemplate: IParamsMappedEntitlement = {
    awardGroupCounter: AwardLimitGroupCodes.BONUS_OVERSEAS_CASH_WITHDRAWAL,
    usageCounters: [
      //usage counters should be already mapped with result from entitlement at this point
      {
        limitGroupCode: 'L006',
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      },
      {
        limitGroupCode: AwardLimitGroupCodes.BONUS_OVERSEAS_CASH_WITHDRAWAL,
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      }
    ]
  };

  const {
    withinQuota: withinQuotaScenario,
    withinQuota2: withinQuotaScenario2,
    exceedQuota: exceedQuota
  } = generateFeeRuleInfoScenarioParams(
    overseasAtmWithdrawalQuota,
    paramsFeeRuleTemplate,
    paramsMappedEntitlementTemplate
  );

  return {
    withinQuota: () => {
      return generateFeeRuleInfoScenarioOutput(
        withinQuotaScenario.paramsFeeRule(),
        withinQuotaScenario.paramsMappedEntitlement()
      );
    },
    withinQuota2: () => {
      return generateFeeRuleInfoScenarioOutput(
        withinQuotaScenario2.paramsFeeRule(),
        withinQuotaScenario2.paramsMappedEntitlement()
      );
    },
    exceedQuota: () => {
      return generateFeeRuleInfoScenarioOutput(
        exceedQuota.paramsFeeRule(),
        exceedQuota.paramsMappedEntitlement()
      );
    }
  };
};

/************************
 * WALLET
 ************************/

export const mockWalletScenario = (walletQuota: number) => {
  const paramsFeeRuleTemplate: IParamsFeeRule = {
    code: FeeRuleType.WALLET_FEE_RULE,
    transactionAmount: 50000,
    monthlyNoTransaction: 0,
    interchange: BankNetworkEnum.IRIS,
    counterCode: COUNTER.COUNTER_01
  };

  const paramsMappedEntitlementTemplate: IParamsMappedEntitlement = {
    awardGroupCounter: AwardLimitGroupCodes.BONUS_TRANSFER,
    usageCounters: [
      //usage counters should be already mapped with result from entitlement at this point
      {
        limitGroupCode: 'L002',
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      },
      {
        limitGroupCode: AwardLimitGroupCodes.BONUS_TRANSFER,
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      }
    ]
  };

  const {
    withinQuota: withinQuotaScenario,
    withinQuota2: withinQuotaScenario2,
    exceedQuota: exceedQuota
  } = generateFeeRuleInfoScenarioParams(
    walletQuota,
    paramsFeeRuleTemplate,
    paramsMappedEntitlementTemplate
  );

  return {
    withinQuota: () => {
      return generateFeeRuleInfoScenarioOutput(
        withinQuotaScenario.paramsFeeRule(),
        withinQuotaScenario.paramsMappedEntitlement()
      );
    },
    withinQuota2: () => {
      return generateFeeRuleInfoScenarioOutput(
        withinQuotaScenario2.paramsFeeRule(),
        withinQuotaScenario2.paramsMappedEntitlement()
      );
    },
    exceedQuota: () => {
      return generateFeeRuleInfoScenarioOutput(
        exceedQuota.paramsFeeRule(),
        exceedQuota.paramsMappedEntitlement()
      );
    }
  };
};

/************************
 * BIFAST
 ************************/
export const mockBIFastScenario = (walletQuota: number) => {
  const paramsFeeRuleTemplate: IParamsFeeRule = {
    code: FeeRuleType.BIFAST_FEE_RULE,
    transactionAmount: 50000,
    monthlyNoTransaction: 0,
    interchange: BankNetworkEnum.IRIS,
    counterCode: COUNTER.COUNTER_01
  };

  const paramsMappedEntitlementTemplate: IParamsMappedEntitlement = {
    awardGroupCounter: AwardLimitGroupCodes.BONUS_TRANSFER,
    usageCounters: [
      //usage counters should be already mapped with result from entitlement at this point
      {
        limitGroupCode: 'L002',
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      },
      {
        limitGroupCode: AwardLimitGroupCodes.BONUS_TRANSFER,
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      }
    ]
  };

  const {
    withinQuota: withinQuotaScenario,
    withinQuota2: withinQuotaScenario2,
    exceedQuota: exceedQuota
  } = generateFeeRuleInfoScenarioParams(
    walletQuota,
    paramsFeeRuleTemplate,
    paramsMappedEntitlementTemplate
  );

  return {
    withinQuota: () => {
      return generateFeeRuleInfoScenarioOutput(
        withinQuotaScenario.paramsFeeRule(),
        withinQuotaScenario.paramsMappedEntitlement()
      );
    },
    withinQuota2: () => {
      return generateFeeRuleInfoScenarioOutput(
        withinQuotaScenario2.paramsFeeRule(),
        withinQuotaScenario2.paramsMappedEntitlement()
      );
    },
    exceedQuota: () => {
      return generateFeeRuleInfoScenarioOutput(
        exceedQuota.paramsFeeRule(),
        exceedQuota.paramsMappedEntitlement()
      );
    }
  };
};
