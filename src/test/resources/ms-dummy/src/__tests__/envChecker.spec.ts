import envChecker from '../envChecker';
import logger from '../logger';
import { requiredEnvVarNames } from '../constant';

describe('envChecker', () => {
  it('should not call process to exist if required env not missing', () => {
    // Given
    requiredEnvVarNames.forEach(envVar => (process.env[envVar] = envVar));

    const spyProccessExit = jest
      .spyOn(process, 'exit')
      .mockImplementation(_ => _ as never);
    const spyLoggerError = jest.spyOn(logger, 'error');

    // When
    envChecker.init();

    // Then
    expect(spyLoggerError).not.toBeCalled();
    expect(spyProccessExit).not.toBeCalled();
  });

  it('should call process to exist if required env missing', () => {
    // Given
    requiredEnvVarNames.forEach(envVar => delete process.env[envVar]);

    const spyProccessExit = jest
      .spyOn(process, 'exit')
      .mockImplementation(_ => _ as never);
    const spyLoggerError = jest.spyOn(logger, 'error');

    // When
    envChecker.init();

    // Then
    expect(spyLoggerError).toBeCalledTimes(requiredEnvVarNames.length);
    expect(spyProccessExit).nthCalledWith(1, 0);
  });
});
