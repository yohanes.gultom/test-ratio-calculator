import kafkaClient from '@dk/module-kafka';
import consumers from '../consumers';
import logger from '../logger';
import { context } from '../context';
import { config } from '../config';
const { groups } = config.get('kafka');
const isLocal = config.get('kafka').isLocal;
const useLocalRedis = config.get('redis').isLocal;

jest.mock('@dk/module-kafka', () => ({
  kafkaConsumer: {
    init: jest.fn(),
    connectSubscribeRun: jest.fn()
  },
  kafkaProducer: {
    init: jest.fn()
  }
}));

describe('Consumers', () => {
  it('should be init valid consumers', async () => {
    const groupIds = [
      groups.directTransactionsBillPayment,
      groups.directTransactionsIris,
      groups.directTransactionsSkn,
      groups.directTransactionsRtgs,
      groups.directTransactionsEn,
      groups.directTransactionsBifast
    ];
    // When
    await consumers.init();

    // Then
    expect(kafkaClient.kafkaConsumer.connectSubscribeRun).toBeCalled();
    expect(kafkaClient.kafkaConsumer.init).toBeCalledWith(
      {
        groupId: groups.transferTransaction,
        isLocal: isLocal,
        useLocalRedis: useLocalRedis,
        maxBytes: config.get('autoTransferTransactionTopicMaxByte')
      },
      logger,
      context
    );

    expect(kafkaClient.kafkaConsumer.init).toBeCalledWith(
      {
        groupId: groups.directTransaction,
        isLocal: isLocal,
        useLocalRedis: useLocalRedis,
        maxBytes: config.get('directTransactionTopicMaxByte')
      },
      logger,
      context
    );

    for (const groupId of groupIds) {
      expect(kafkaClient.kafkaConsumer.init).toBeCalledWith(
        {
          groupId: groupId,
          isLocal: isLocal,
          useLocalRedis: useLocalRedis
        },
        logger,
        context
      );
    }

    expect(kafkaClient.kafkaConsumer.init).toBeCalledTimes(8);
  });
});
