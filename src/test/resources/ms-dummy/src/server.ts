import hapi from '@hapi/hapi';
import * as Inert from '@hapi/inert';
import * as Vision from '@hapi/vision';
import kafkaProducer from './common/kafkaProducer';

import { connectMongo } from './common/mongoDb';
import errorHandler from './common/handleValidationErrors';
import Swagger from './plugins/swagger';
import ResponseWrapper from './plugins/responseWrapper';
import RequestWrapper from './plugins/requestWrapper';
import './joiInitializer';
import { routes } from './routes';
import logger from './logger';
import { config } from './config';
import consumers from './consumers';
import { aclWrapper, HapiPlugin } from '@dk/module-common';
import TokenAuth from './plugins/tokenAuth';
import envChecker from './envChecker';

const { port, host } = config.get('server');

const createServer = async () => {
  const server = new hapi.Server({
    port,
    host,
    routes: {
      validate: {
        options: {
          abortEarly: false
        },
        failAction: errorHandler
      }
    }
  });

  const plugins: any[] = [
    Inert,
    Vision,
    Swagger,
    HapiPlugin.Good(logger),
    ResponseWrapper,
    RequestWrapper,
    TokenAuth,
    aclWrapper
  ];
  await server.register(plugins);

  // Register routes
  server.route(routes);

  return server;
};

export const init = async () => {
  await connectMongo();
  const server = await createServer();
  await server
    .initialize()
    .then(() =>
      logger.info(`server started at ${server.info.host}:${server.info.port}`)
    );
  return server;
};

export const start = async (module: NodeModule) => {
  if (!module.parent) {
    logger.info('Start server');
    envChecker.init();
    await init()
      .then(async server => {
        await kafkaProducer.connect();
        await consumers.init();
        await server.start();
      })
      .catch(err => {
        logger.error('Server cannot start', err);
        logger.onFinished(() => {
          process.exit(1);
        });
      });
  }
};

start(module);
