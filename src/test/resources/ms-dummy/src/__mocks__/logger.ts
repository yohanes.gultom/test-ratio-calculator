const wrapLogs = jest.fn(obj => obj);
const logger = {
  error: jest.fn(),
  info: jest.fn(),
  debug: jest.fn(),
  warn: jest.fn(),
  critical: jest.fn()
};

export { wrapLogs };
export default logger;
