import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class TestRatioCalculator {

    public long countLine(String fileName) throws IOException {
        long lines;
        try (InputStream is = new BufferedInputStream(new FileInputStream(fileName))) {
            byte[] c = new byte[1024];
            int count = 0;
            int readChars;
            boolean endsWithoutNewLine = false;
            while ((readChars = is.read(c)) != -1) {
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n')
                        ++count;
                }
                endsWithoutNewLine = (c[readChars - 1] != '\n');
            }
            if (endsWithoutNewLine) {
                ++count;
            }
            lines = count;
        }
        return lines;
    }

    public String getFileExtension(String filename) {
        return filename != null && !filename.startsWith(".") ?
                filename.substring(filename.lastIndexOf(".") + 1) : "";
    }

    public boolean isTestFile(String filePath) {
        File file = new File(filePath);
        return file.getName().contains(".spec");
    }

    public boolean isAppFile(String filePath) {
        File file = new File(filePath);
        String ext = getFileExtension(file.getName()).toLowerCase();
        return Arrays.asList("java", "ts", "js").contains(ext);
    }

    public TestRatio calculateTestRatio(String dir) throws IOException {
        return calculateTestRatio(dir, false);
    }

    public TestRatio calculateTestRatio(String dir, boolean isUnderTestDir) throws IOException {
        TestRatio testRatio = new TestRatio();
        Path pathDir = Paths.get(dir);
        String baseDirName = pathDir.getName(pathDir.getNameCount() - 1).toString();
        if (Arrays.asList("node_modules", "lib", "dist", "target", "build", "resources")
                .contains(baseDirName.toLowerCase())) {
            return testRatio;
        }
        boolean isTestDir = isUnderTestDir
                || (Arrays.asList("__mocks__", "__jest__", "__tests__", "test").contains(baseDirName.toLowerCase()));
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(pathDir)) {
            for (Path path : stream) {
                if (Files.isDirectory(path)) {
                    TestRatio subDirRatio = calculateTestRatio(path.toString(), isTestDir);
                    testRatio.addAppFileCount(subDirRatio.getAppFileCount());
                    testRatio.addAppLineCount(subDirRatio.getAppLineCount());
                    testRatio.addTestFileCount(subDirRatio.getTestFileCount());
                    testRatio.addTestLineCount(subDirRatio.getTestLineCount());
                    testRatio.addOtherFileCount(subDirRatio.getOtherFileCount());
                } else {
                    String filePath = path.toString();
                    if ((isTestDir && isAppFile(filePath)) || isTestFile(filePath)) {
                        testRatio.addTestFileCount(1);
                        testRatio.addTestLineCount(countLine(filePath));
                    } else if (isAppFile(filePath)) {
                        testRatio.addAppFileCount(1);
                        testRatio.addAppLineCount(countLine(filePath));
                    } else {
                        testRatio.addOtherFileCount(1);
                    }
                }
            }
        }
        return testRatio;
    }

    public static void main(String[] args) throws IOException {
        TestRatioCalculator testRatioCalculator = new TestRatioCalculator();
        System.out.println(args[0]);
        TestRatio testRatio = testRatioCalculator.calculateTestRatio(args[0]);
        System.out.println(testRatio.toString());
    }
}
