public class TestRatio {
    private long appLineCount;
    private long testLineCount;

    private long appFileCount;

    private long testFileCount;

    private long otherFileCount;

    public TestRatio() {
        this.appFileCount = 0;
        this.appLineCount = 0;
        this.testFileCount = 0;
        this.testLineCount = 0;
        this.otherFileCount = 0;
    }

    public TestRatio(long appFileCount, long appLineCount, long testFileCount, long testLineCount, long otherFileCount) {
        this.appFileCount = appFileCount;
        this.appLineCount = appLineCount;
        this.testFileCount = testFileCount;
        this.testLineCount = testLineCount;
        this.otherFileCount = otherFileCount;
    }

    public long getAppFileCount() {
        return appFileCount;
    }

    public void addAppFileCount(long value) {
        this.appFileCount += value;
    }

    public long getAppLineCount() {
        return appLineCount;
    }

    public void addAppLineCount(long value) {
        this.appLineCount += value;
    }

    public long getTestFileCount() {
        return testFileCount;
    }

    public void addTestFileCount(long value) {
        this.testFileCount += value;
    }

    public long getTestLineCount() {
        return testLineCount;
    }

    public void addTestLineCount(long value) {
        this.testLineCount += value;
    }

    public long getOtherFileCount() {
        return otherFileCount;
    }

    public void addOtherFileCount(long value) {
        this.otherFileCount += value;
    }

    public double getRatio() {
        return this.appLineCount > 0 ? (double) this.testLineCount / this.appLineCount : 0.0;
    }

    @Override
    public String toString() {
        return String.format(
                "Total app file: %d\nTotal test file: %d\nTotal other file: %d\nTotal app lines: %d\nTotal test lines:%d\nTest-app line ratio: %f",
                getAppFileCount(), getTestFileCount(), getOtherFileCount(), getAppLineCount(), getTestLineCount(), getRatio());
    }
}
