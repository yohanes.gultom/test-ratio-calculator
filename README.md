# Test Ratio Calculator

Calculate ratio between test files and app files inside a Node.js project directory

## Build

```bash
docker run --rm -v $(pwd):/home/app -w /home/app maven:3-openjdk-17 mvn clean package
```

Uber/fat JAR will be generated at `/target/test-ratio-calculator-{version}-uber.jar`

## Run

Assuming the project is located in `/Users/yohanes/Workspace/ms-transfer` and the JAR version is `1.0`:

```bash
docker run --rm -v /Users/yohanes/Workspace/ms-transfer:/home/app/project -v $(pwd)/target/test-ratio-calculator-1.0-uber.jar:/home/app/app.jar -w /home/app maven:3-openjdk-17 java -jar app.jar project
```
